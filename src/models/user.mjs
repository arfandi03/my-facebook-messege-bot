'use strict';
import { Model } from 'sequelize'
export default (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasMany(models.Message, { as: "messages" })
    }
  }
  User.init({
    user_id: DataTypes.STRING,
    user_name: DataTypes.STRING,
    user_birthday: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'User',
    underscored: true
  });
  return User;
};