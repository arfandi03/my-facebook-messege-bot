
import { getAllMessage, getMessage} from "../services/messageService.mjs"
import { getSummary } from "../services/userService.mjs"

let listMessage = async (req, res) => {
    let messages = await getAllMessage();
    res.status(200).send(messages);
};

let showMessage = async (req, res) => {
    try{
        let message = await getMessage(req.params.id);
        res.status(200).send(message);
    }
    catch(e){
        res.status(404).send(e);
    }
};

let summaryMessage = async (req, res) => {
    let summary = await getSummary();
    res.status(200).send(summary);
};

export { listMessage, showMessage, summaryMessage };