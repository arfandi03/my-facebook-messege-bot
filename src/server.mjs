import express from "express"
import initWebRoute from "./routes/web.mjs"
import bodyParser from "body-parser"
import "dotenv/config"
import sequelize from "./config/database.mjs"

const app = express()

// use body-parser ro post data
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// init all web route
initWebRoute(app)

let port = process.env.PORT || 8000

sequelize.sync().then(() => { console.log("DB Connected") });

app.listen(port, () => console.log(`App is running at the port ${port}`))