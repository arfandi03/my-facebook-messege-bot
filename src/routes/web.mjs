import express from "express"
import * as chatbotController from "../controllers/chatbotController.mjs"
import * as messageController from "../controllers/messageController.mjs"

let router = express.Router()

let initWebRoutes = (app) => {
    router.get("/webhook", chatbotController.getWebhook)
    router.post("/webhook", chatbotController.postWebhook)

    router.get("/messages", messageController.listMessage)
    router.get("/messages/:id", messageController.showMessage)
    router.get("/summary", messageController.summaryMessage)

    return app.use("/", router)
};

export default initWebRoutes