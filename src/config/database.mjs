import Sequelize from "sequelize"
import fs from "fs"

const dbConfig = JSON.parse(fs.readFileSync('src/config/config.json', 'utf8'))['development'];

const sequelize = new Sequelize(dbConfig.database, dbConfig.username, dbConfig.password, {
  dialect: dbConfig.dialect,
  host: dbConfig.host,
  storage: `./src/${dbConfig.storage}`,
  // logging: dbConfig.logging,
});

export default sequelize