import db from "../models/index.mjs"

const Message = db.Message

const createMessage = async (body) => {
    console.log("createMessage", body);
    await Message.create(body)
}

const getMessage = async (id) => {
    const message = await Message.findOne({ where: { id: id } });
    if(!message) {
        throw new MessageNotFoundException();
    }
    return message;
}

const getAllMessage = async (id) => {
    return await Message.findAll();
    // return await Message.findAll({ include: ["comments"], });
}

function MessageNotFoundException(){
    this.status = 404;
    this.message = 'Message not found';
}

export { createMessage, getMessage, getAllMessage }