import axios from "axios";
import "dotenv/config" 
import * as userService from "./userService.mjs"
import * as messageService from "./messageService.mjs"

// Handles messaging_postbacks events
async function handlePostback(sender_psid, received_postback) {
    let response = { "text": "under development!" };
    callSendAPI(sender_psid, response);
}

async function handleMessage(sender_psid, message) {
    let entitiesArr = [ "wit$greetings", "wit$thanks", "wit$bye", "wit$datetime:datetime" ];
    let entityChosen = "";
    let response;
    let user = await userService.getUser(sender_psid)
    let userProfile = await getUserProfile(sender_psid)

    entitiesArr.forEach((name) => {
        let entity = firstTrait(message.nlp, name);
        if (entity && entity.confidence > 0.8) entityChosen = name;
    });

    if(entityChosen === "wit$greetings"){
        //send greetings message
        response = { "text": "Hi there! what is your first name?" }
    }
    else if(entityChosen === "wit$thanks"){
        //send thanks message
        response = { "text":"You're welcome!" }
    }
    else if(entityChosen === "wit$bye"){
        //send bye message
        response = { "text": "Bye-bye!" }
    }
    else{
        if (new RegExp('^' + userProfile.first_name + '$', 'i').test(message.text)) {
            response = {  "text": "When is your birthday?" }
        }
        else if(entityChosen === "wit$datetime:datetime"){
            let date = new Date(message.text);
            userService.setUserBirthday(sender_psid, date)
            response = quickReplies("Do you want to know how many days until your next birthday?")
        }
        else{
            response = { "text": `The bot is needed more training, try to say "thanks a lot" or "hi" to the bot` };
        }
        // Set the response based on the payload
        if (message.quick_reply && message.quick_reply.payload  === 'quick_replies_yes') {
            response = { "text": calculateBirthday(user.user_birthday) }
        } else if (message.quick_reply && message.quick_reply.payload === 'quick_replies_no') {
            response = { "text": "Goodbye" }
        }
    }

    saveMessage(sender_psid, message.text);
    callSendAPI(sender_psid, response);
}

function firstTrait(nlp, name) {
    if(nlp && nlp.entities && nlp.traits[name]) return nlp.traits[name][0]
    else if(nlp && nlp.entities && nlp.entities[name]) return nlp.entities[name][0]
    
    return false
}

function quickReplies(text){
    return {
        "text": text,
        "quick_replies":[
            {
                "content_type":"text",
                "title":"Yes",
                "payload":"quick_replies_yes",
            },
            {
                "content_type":"text",
                "title":"No",
                "payload":"quick_replies_no",
            }
        ]
    }
}

// Sends response messages via the Send API
async function callSendAPI(sender_psid, message) {
    // Construct the message body
    let request_body = {
        "recipient": {
            "id": sender_psid
        },
        message,
    };

    try {
        await axios.post(`https://graph.facebook.com/v7.0/me/messages`, request_body, { 
            params: {
                access_token: process.env.FB_PAGE_TOKEN, 
            }
        })
        console.log('message sent!');
    } catch (error) {
        console.error("Unable to send message:" + error);
    }
}

async function saveMessage(sender_psid, text){
    try {
        let user = await userService.getUser(sender_psid)
        if(!user){
            let userProfile = await getUserProfile(sender_psid)
            user = await userService.createUser({
                user_id: sender_psid,
                user_name: `${userProfile.name}`
            })
        }
        messageService.createMessage({text: text, user_id: user.id})
    }
    catch(err) {
        console.log(`Something error: ${err}`);
    }
}

async function getUserProfile(sender_psid) {
    try {
        const { data } = await axios.get(`https://graph.facebook.com/${sender_psid}`, { 
            params: {
                fields: "name,first_name,last_name,gender",
                access_token: process.env.FB_PAGE_TOKEN, 
            }
        })
        return data
    } catch (error) {
        console.error(error);
    }
}

function calculateBirthday(birthDay) {
    let today = new Date();
    let bday = new Date(birthDay); 
    let age = today.getFullYear() - bday.getFullYear();
    
    let upcomingBday = new Date(today.getFullYear(), bday.getMonth(), bday.getDate());
    
    if(today > upcomingBday) {
        upcomingBday.setFullYear(today.getFullYear() + 1);
    }
    
    let one_day = 24 * 60 * 60 * 1000;
    
    let daysLeft = Math.ceil((upcomingBday.getTime() - today.getTime()) / (one_day));
    
    return (daysLeft && age) ?  `In ${daysLeft} day(s), you will be ${age + 1}!` : "Please enter a valid birtday.";  
}

export { handleMessage, handlePostback };