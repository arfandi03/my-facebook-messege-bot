import db from "../models/index.mjs"

const User = db.User

const createUser = async (body) => await User.create(body)

const getUser = async (user_id) => {
    return await User.findOne({ where: { user_id: user_id } })
}

const getSummary = async () => {
    return await User.findAll({ include: ["messages"] });
}

const setUserBirthday = async (user_id, user_birthday) => {
    const user = await User.findOne({ where: { user_id: user_id } });
    user.user_birthday = user_birthday;
    await user.save();
}

export { createUser, getUser, getSummary, setUserBirthday }