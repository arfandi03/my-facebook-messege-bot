## Project setup
#### 1. Clone the repo and install dependencies
```bash
git clone 
cd facebook-message-bot
npm install
```
#### 2. Setup ngrok, then run
```bash
./ngrok http 8000 -> change with server port
```
#### 3. Setup Facebook Page, Developer Account, and Facebook Messanger App
note: enable Built-in NLP in facebook messanger app
#### 4. Modify the .env file
Save `.env.example` as `.env` and modify it
#### 5. Migrate database
```
npm run migrate
```
#### 6. Run server
```
npm run dev
```
